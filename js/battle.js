console.log("Let's get a battle!");

var config = {
    type: Phaser.AUTO,
    parent: 'game',
    width: window.innerWidth,
    height: window.innerHeight,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
            gravity: { y: 0 }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);

function preload() {

    // sprites
    this.load.image('ship', 'assets/space-ship-01.png');
    this.load.image('enemy', 'assets/space-ship-02.png');
    this.load.image('collectable', 'assets/collectable.png');
    this.load.image('trap', 'assets/dont.png');
    this.load.image('stage', 'assets/background.jpg');

    // audio
    this.load.audio('trap', 'assets/trap.ogg');
    this.load.audio('collected', 'assets/collected.ogg');
    this.load.audio('gameplay', 'assets/BossMain.wav');
}

function create() {
    this.socket = io();
    this.teamAScore;
    this.teamBScore;
    self = this;

    var gameTrack = this.sound.add('gameplay');

    gameTrack.play({
        mute: false,
        volume: 1,
        rate: 1,
        detune: 0,
        seek: 0,
        loop: true,
        delay: 0
    });

    this.add.image(0, 0, 'stage').setDisplaySize(window.innerWidth + window.innerWidth, window.innerHeight + window.innerHeight);

    configureHUPDisplay(self);

    this.socket.emit("player-connected", {
        screenWidth: window.innerWidth,
        screenHeight: window.innerHeight
    });

    this.otherPlayers = this.physics.add.group();

    this.socket.on('currentPlayers', function (players) {
        Object.keys(players).forEach(function (id) {
            if (players[id]._id === self.socket.id) {
                addPlayer(self, players[id]);
            } else {
                addOtherPlayers(self, players[id]);
            }
        });
    });

    this.socket.on('newPlayer', function (playerInfo) {
        addOtherPlayers(self, playerInfo);
    });

    this.socket.on('playerMoved', function (playerInfo) {
        self.otherPlayers.getChildren().forEach(function (otherPlayer) {
            if (playerInfo.playerId === otherPlayer.playerId) {
                otherPlayer.setRotation(playerInfo.rotation);
                otherPlayer.setPosition(playerInfo.x, playerInfo.y);
            }
        });
    });

    this.socket.on('disconnect', function (playerId) {
        self.otherPlayers.getChildren().forEach(function (otherPlayer) {
            if (playerId === otherPlayer.playerId) {
                otherPlayer.destroy();
            }
        });
    });

    this.socket.on('scoreUpdate', function (scores) {
        self.teamAScore.setText('Time A: ' + scores.blue);
        self.teamBScore.setText('Time B: ' + scores.red);
    });

    this.socket.on('starLocation', function (starLocation) {
        if (self.star) self.star.destroy();
        self.star = self.physics.add.image(starLocation.x, starLocation.y, 'collectable');
        self.physics.add.overlap(self.ship, self.star, function () {
            this.socket.emit('starCollected');
            this.sound.play('collected');
        }, null, self);
    });

    this.socket.on('trapLocation', function (trapLocation) {
        if (self.trap) self.trap.destroy();
        self.trap = self.physics.add.image(trapLocation.x, trapLocation.y, 'trap').setDisplaySize(50, 50);
        self.physics.add.overlap(self.ship, self.trap, function () {
            this.socket.emit('trapped');
            this.sound.play('trap');
        }, null, self);
    })
    this.cursors = this.input.keyboard.createCursorKeys();
}

function update() {

    if (this.ship) {

        var x = this.ship.x;
        var y = this.ship.y;
        var r = this.ship.rotation;
        if (this.ship.oldPosition && (x !== this.ship.oldPosition.x || y !== this.ship.oldPosition.y || r !== this.ship.oldPosition.rotation)) {
            this.socket.emit('playerMovement', { x: this.ship.x, y: this.ship.y, rotation: this.ship.rotation });
        }

        this.ship.oldPosition = {
            x: this.ship.x,
            y: this.ship.y,
            rotation: this.ship.rotation
        };

        if (this.cursors.left.isDown) {
            this.ship.setAngularVelocity(-500);
        } else if (this.cursors.right.isDown) {
            this.ship.setAngularVelocity(500);
        } else {
            this.ship.setAngularVelocity(0);
        }

        if (this.cursors.up.isDown) {
            this.physics.velocityFromRotation(this.ship.rotation + 1.5, 500, this.ship.body.acceleration);
        } else {
            this.ship.setAcceleration(0);
        }

        this.physics.world.wrap(this.ship, 5);
    }
}

function addPlayer(self, playerInfo) {
    self.ship = self.physics.add.image(playerInfo.x, playerInfo.y, 'ship').setOrigin(0.5, 0.5).setDisplaySize(53, 40);
    self.ship.setDrag(100);
    self.ship.setAngularDrag(100);
    self.ship.setMaxVelocity(200);
}

function addOtherPlayers(self, playerInfo) {
    const otherPlayer = self.add.sprite(playerInfo.x, playerInfo.y, 'enemy').setOrigin(0.5, 0.5).setDisplaySize(53, 40);
    otherPlayer.playerId = playerInfo.playerId;
    self.otherPlayers.add(otherPlayer);
}

function configureHUPDisplay(self) {
    self.headerScore = self.add.text(36, 16, '', { fontFamily: 'Arial', fontSize: '25px', fill: '#A1FBEE' });
    self.headerScore.setText("Score");

    self.teamAScore = self.add.text(36, 52, '', { fontFamily: 'Arial', fontSize: '22px', fill: '#A1FBEE' });
    self.teamBScore = self.add.text(36, 80, '', { fontFamily: 'Arial', fontSize: '22px', fill: '#A1FBEE' });
}