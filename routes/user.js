const express = require('express');
const router = express.Router();
const path = require('path');

router.get('/', (req, res) => {
    res.sendFile(path.join(path.dirname(__dirname), 'menu.html' ));
});

module.exports = router;


