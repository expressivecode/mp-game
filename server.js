const express = require('express');
const game = express();
const app = require('http').createServer(game);
const io = require('socket.io')(app);
const battlesRoutes = require('./routes/battles.js');
const userRoutes = require('./routes/user.js');

const players = {};

const star = {
    x: Math.floor(Math.random() * 700) + 50,
    y: Math.floor(Math.random() * 500) + 50
};

const trap = {
    x: Math.floor(Math.random() * 376) + 27,
    y: Math.floor(Math.random() * 178) + 33,
}

const scores = {
    blue: 0,
    red: 0
};


game.use('/assets', express.static('assets'));
game.use('/phaser', express.static('node_modules/phaser/dist'));
game.use('/socket.io', express.static('node_modules/socket.io-client/dist'));
game.use('/js', express.static('js'));

game.use('/', userRoutes);
game.use('/start', battlesRoutes);

app.listen(process.env.PORT || 3000, () => {
    console.log("The battle server was started. Go ahead!");

    io.on('connection', (socket) => {

        socket.on('player-connected', function (playerData) {

            players[socket.id] = {
                _id: socket.id,
                rotation: 0,
                x: Math.floor(Math.random() * playerData.screenWidth) + 50,
                y: Math.floor(Math.random() * playerData.screenHeight) + 50,
                team: (Math.floor(Math.random() * 2) == 0) ? 'A' : 'B'
            };


            socket.emit('currentPlayers', players);

            socket.emit('starLocation', star);

            socket.emit('scoreUpdate', scores);

            socket.emit('trapLocation', trap);

            socket.broadcast.emit('newPlayer', players[socket.id]);

            console.log("Player connected with id:", players[socket.id]._id);

            socket.on('disconnect', () => {

                delete players[socket.id];

                if ( Object.keys(players).length === 0 ) {

                    star.x = Math.floor(Math.random() * 700) + 50,
                    star.y = Math.floor(Math.random() * 500) + 50

                    trap.x = Math.floor(Math.random() * 376) + 27,
                    trap.y = Math.floor(Math.random() * 178) + 33,

                    scores.blue = 0;
                    scores.red = 0;
                }

                io.emit('disconnect', socket.id);
                console.log('Player disconnected.');
            });

            socket.on('playerMovement', function (movementData) {
                players[socket.id].x = movementData.x;
                players[socket.id].y = movementData.y;
                players[socket.id].rotation = movementData.rotation;
                socket.broadcast.emit('playerMoved', players[socket.id]);
            });

            socket.on('starCollected', function () {
                if (players[socket.id].team === 'A') {
                    scores.red += 10;
                } else {
                    scores.blue += 10;
                }
                star.x = Math.floor(Math.random() * 740) + 80;
                star.y = Math.floor(Math.random() * 300) + 80;
                io.emit('starLocation', star);
                io.emit('scoreUpdate', scores);
            });

            socket.on('trapped', function(){
                if (players[socket.id].team === 'A') {
                    scores.red += 10;
                } else {
                    scores.blue += 10;
                }
                trap.x = Math.floor(Math.random() * playerData.screenWidth) + 50,
                trap.y = Math.floor(Math.random() * playerData.screenHeight) + 50,
                io.emit('trapLocation', trap);
                io.emit('scoreUpdate', scores);
            })

        });
    });
})